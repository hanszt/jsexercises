const continuationBtn = document.querySelector('.continuationBtn');

const activateContinuation = function () {
    for (const e of generator()) {
        console.info(`in the loop with ${e}`);
    }
};

continuationBtn.addEventListener('click', activateContinuation);

const generator = function* () {
    const something = 7;
    console.info(`entering...`);
    yield 1;
    console.info(`step 1`);
    yield 2;
    console.info(`step 2`);
    yield 3;
    console.info(`step 3`);
    yield 4;
    console.info(`step 4`);
    yield something;
};
