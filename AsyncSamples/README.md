## Async samples

This is a project to practice async and functional programming in javascript.

## Sources
- [What is a continuation?](https://www.youtube.com/watch?v=zB5LTkaJaqk)
- [Callbacks like javascript](https://youtu.be/VRpHdSFWGPs?t=6937)