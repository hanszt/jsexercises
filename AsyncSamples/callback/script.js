const callbackBtn = document.querySelector('.callbackBtn');
const fullNameBtn = document.querySelector('.fullNameBtn');

const testCallback = function () {
    hello('John', null, () => console.info('No last name provided.'));
};
callbackBtn.addEventListener('click', testCallback);

const printFullName = function () {
    hello('John', 'Deer', () => console.info('No last name provided.'));
};

fullNameBtn.addEventListener('click', printFullName);

const hello = function (firstName, lastName, callback) {
    console.info(firstName);
    if (lastName) {
        console.info(lastName);
    } else {
        callback();
    }
};
