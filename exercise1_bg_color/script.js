const colorBtn = document.querySelector('.colorBtn');
const bodyBackground = document.querySelector('body');

const colors = ['yellow', 'red', 'green', '#3b5998'];

const changeColor = () => {
    // Math.floor rounds down to the nearest integer
    const random = Math.floor(Math.random() * colors.length);
    bodyBackground.style.backgroundColor = colors[random];
};

colorBtn.addEventListener('click', changeColor);
