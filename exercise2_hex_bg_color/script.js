const hexBtn = document.querySelector('.hexBtn');
const bodyBackground = document.querySelector('body');
const hex = document.querySelector('.hex');
const hexNumbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F'];

const DIGIT_AMOUNT_HEX_COLOR = 6;

const changeHexColor = () => {
    let hexCol = '#';
    for (let i = 0; i < DIGIT_AMOUNT_HEX_COLOR; i++) {
        // Math.floor rounds down to the nearest integer
        const random = Math.floor(Math.random() * hexNumbers.length);
        hexCol += hexNumbers[random];
    }
    bodyBackground.style.backgroundColor = hexCol;
    hex.innerHTML = hexCol;
};

hexBtn.addEventListener('click', changeHexColor);
