const navbarBtn = document.querySelector('.navbar_btn');
const navbarLinks = document.querySelector('.navbar_links');
const NAV_BAR_COLLAPSE = 'navbar_collapse';
const CHANGE = 'change';


const moveMenu = () => {
    navbarLinks.classList.toggle(NAV_BAR_COLLAPSE);
    navbarBtn.classList.toggle(CHANGE);
};

navbarBtn.addEventListener('click', () => moveMenu());
