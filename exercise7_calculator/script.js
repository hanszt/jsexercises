const buttons = document.querySelectorAll('.btn');
const screen = document.querySelector('.screen');
const equalBtn = document.querySelector('.btn-equal');
const clearBtn = document.querySelector('.btn-clear');

for (const button of buttons) {
    button.addEventListener('click', () => {
        const sign = button.getAttribute('data-num');
        screen.value += sign;
    });
}

equalBtn.addEventListener('click', () => {
    if (screen.value === '') {
        alert('input is empty');
    } else {
        screen.value = eval(screen.value);
    }
});

clearBtn.addEventListener('click', () => screen.value = '');
