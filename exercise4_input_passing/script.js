const sendBtn = document.querySelector('#sendBtn');
const messageIn = document.querySelector('#messageIn');
const messageOut = document.querySelector('#messageOut');


const sendMessage = () => {
    const content = messageIn.value;
    if (content === '') {
        alert('Please enter a valid value. Current value is empty');
    } else {
        messageOut.innerHTML = content;
        messageIn.value = '';
    }
};

sendBtn.addEventListener('click', sendMessage);
