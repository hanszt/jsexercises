const counter = document.querySelector('.counter');
const addCount = document.querySelector('#addCountBtn');
const lowerCount = document.querySelector('#lowerCountBtn');

let count = 0;

const setCounterColor = () => {
    if (count > 0) {
        counter.style.color = '#4caf50';
    } else if (count === 0) {
        counter.style.color = 'white';
    } else {
        counter.style.color = 'red';
    }
};

const animateCounter = animationDuration => {
    counter.innerHTML = String(count);
    setCounterColor();
    counter.animate([{opacity: '0.2'}, {opacity: '1.0'}],
        {duration: animationDuration, fill: 'forwards'});
};

const incrementCounter = () => {
    count++;
    const animationDuration = 800;
    animateCounter(animationDuration);
};

addCount.addEventListener('click', incrementCounter);

const decrementAnimationDuration = 500;

const decrementCounter = () => {
    count--;
    animateCounter(decrementAnimationDuration);
};

lowerCount.addEventListener('click', decrementCounter);

animateCounter(decrementAnimationDuration);
