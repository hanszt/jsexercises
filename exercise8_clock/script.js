const NOON_HOUR = 12;
const convertFormat = hours => (hours >= NOON_HOUR) ? 'PM' : 'AM';

const checkTime = hours => {
    if (hours > NOON_HOUR) {
        hours = hours - NOON_HOUR;
    }
    if (hours === 0) {
        hours = NOON_HOUR;
    }
    return hours;
};

const addZero = time => {
    const TEN = 10;
    if (time < TEN) {
        time = '0' + time;
    }
    return time;
};

const showTime = () => {
    const date = new Date();
    let hours = date.getHours();//0-23
    let minutes = date.getMinutes();//0-59
    let seconds = date.getSeconds();//0-59
    hours = checkTime(hours);
    hours = addZero(hours);
    minutes = addZero(minutes);
    seconds = addZero(seconds);
    const formatHours = convertFormat(hours);
    document.getElementById('clock')
        .innerHTML = `${hours} : ${minutes} : ${seconds} ${formatHours}`;
};

// this is to see the clock immediately after refreshing
showTime();

const timeout = 1000;
setInterval(showTime, timeout);
