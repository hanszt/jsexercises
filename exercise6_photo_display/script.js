const prevButton = document.querySelector('.prevBtn');
const nextButton = document.querySelector('.nextBtn');
const container = document.querySelector('.images');

let counter = 0;

const xmlHttp = new XMLHttpRequest();

xmlHttp.onreadystatechange = function () {
    const statusOk = 200;
    if (xmlHttp.readyState === XMLHttpRequest.DONE && xmlHttp.status === statusOk) {
        const size = (xmlHttp.responseText.match(/jpg/g) || []).length;
        console.log(size);
    }
};

xmlHttp.open('GET', '../exercise6_photo_display/images/', true);
xmlHttp.send();

const NUMBER_OF_IMAGES = 3;
const nextSlide = () => {
    counter++;
    counter = counter % NUMBER_OF_IMAGES;
    container.animate([{opacity: '0.1'}, {opacity: '1.0'}], {duration: 1000, fill: 'forwards'});
    container.style.backgroundImage = `url(images/DJI_${counter}.jpg`;
};

nextButton.addEventListener('click', nextSlide);

const prevSlide = () => {
    if (counter === 0) {
        counter = NUMBER_OF_IMAGES;
    }
    counter--;
    container.animate([{opacity: '0.1'}, {opacity: '1.0'}], {duration: 800, fill: 'forwards'});
    container.style.backgroundImage = `url(images/DJI_${counter}.jpg`;
};

prevButton.addEventListener('click', prevSlide);
