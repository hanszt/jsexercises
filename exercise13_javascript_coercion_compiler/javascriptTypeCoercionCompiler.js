const INPUT = document.getElementById('text-input');
const PRINT_BUTTON = document.getElementById('print');
const JAVASCRIPT_INPUT = document.getElementById('javascript-input');
const COMPILE_BUTTON = document.getElementById('compile');

const zero = '+[]';
const one = '+!![]';
const emptyString = '[]+[]';

const number = n => n === 0 ? zero : Array.from({length: n}, () => one).join(' + ');

const toCharacter = c => {
    if (!(c in map)) {
        const charCode = c.charCodeAt(0);
        return `(${emptyString})[${fromString('constructor')}][${fromString('fromCharCode')}](${number(charCode)})`;
    }
    return map[c];
};

const fromString = s => s.split('')
    .map(toCharacter)
    .join('+');

// fromChadetsingcup[space][backslash]
// with these characters we can encode anything in javascript
// [object Object]
const objectSpaceObject = '({}+[])';
const falseAsString = `!${emptyString}`;
const trueAsString = `!${falseAsString}`;

const map = {
    b: `${objectSpaceObject}[${number(2)}]`,
    o: `${objectSpaceObject}[${one}]`,
    e: `${objectSpaceObject}[${number(4)}]`,
    c: `${objectSpaceObject}[${number(5)}]`,
    t: `${objectSpaceObject}[${number(6)}]`,
    a: `(${falseAsString})[${one}]`,
    f: `(${falseAsString})[${zero}]`,
    s: `(${falseAsString})[${number(3)}]`,
    r: `(${trueAsString})[${one}]`,
    u: `(${trueAsString})[${number(2)}]`,
    i: `(${one}/${zero}+[])[${number(3)}]`,
    n: `(${one}/${zero}+[])[${number(4)}]`,
    [' ']: `(${objectSpaceObject})[${number(7)}]`,
    ['\\']: `(/\\\\/+[])[${one}]`
};
const stringConstructor = `[]+(${emptyString})[${fromString('constructor')}]`;
map.S = `(${stringConstructor})[${number(9)}]`;
map.g = `(${stringConstructor})[${number(14)}]`;

const regexConstructor = `[]+(/-/)[${fromString('constructor')}]`;
map.p = `(${regexConstructor})[${number(14)}]`;

//using the toString function with base: For example: (13).toString(14);
map.d = `(${number(13)})[${fromString('toString')}](${number(14)})`;
map.h = `(${number(17)})[${fromString('toString')}](${number(18)})`;
map.m = `(${number(22)})[${fromString('toString')}](${number(23)})`;

const functionConstructor = `()=>{}`;
// The escapedBackslash is the hexadecimalNr: %5C
const escapedBackslash = `(${functionConstructor})[${fromString('constructor')}](${fromString('return escape')})()(${map['\\']})`;
map.C = `(${escapedBackslash})[${number(2)}]`;

const compile = code => `(${functionConstructor})[${fromString('constructor')}](${fromString(code)})()`;

const logCompiledCodeToConsole = () => console.info(compile(JAVASCRIPT_INPUT.value));
const logToConsole = () => console.info(fromString(INPUT.value));

COMPILE_BUTTON.addEventListener('click', logCompiledCodeToConsole);
PRINT_BUTTON.addEventListener('click', logToConsole);
