# Exercises HTML, CSS, Javascript: Basic javascript projects

---

A project to learn concepts of basic html, css and javascript.

----

## sources

- [Basic Javascript Projects](https://www.youtube.com/watch?v=Kp3HGwlXwCk)
- [15 Vanilla Javascript Projects](https://youtu.be/c5SIG7Ie0dM)
- [All Projects](https://www.johnsmilga.com/projects)
