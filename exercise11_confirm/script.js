const counter = document.querySelector('.counter');
const addCount = document.querySelector('#incrementBtn');
const lowerCount = document.querySelector('#decrementBtn');
const positiveCountField = document.getElementById('setCounterPositive');
const negativeCountField = document.getElementById('setCounterNegative');

let count = 0;

const setCounterColor = () => {
    if (count > 0) {
        counter.style.color = '#4caf50';
    } else if (count === 0) {
        counter.style.color = 'white';
    } else {
        counter.style.color = 'red';
    }
};

const setCountIfConfirmed = (evt, value) => {
    const confirm = window.confirm(`Sure you want to set the counter to ${value} ?`);
    if (confirm) {
        count = Number.parseInt(value, 10);
        animateCounter(500);
    }
    evt.stopPropagation();
};

positiveCountField.addEventListener('change', evt => setCountIfConfirmed(evt, positiveCountField.value));
negativeCountField.addEventListener('change', evt => setCountIfConfirmed(evt, negativeCountField.value));

const animateCounter = animationDuration => {
    counter.innerHTML = String(count);
    setCounterColor();
    counter.animate([{opacity: '0.2'}, {opacity: '1.0'}],
        {duration: animationDuration, fill: 'forwards'});
};

const incrementCounter = () => {
    count++;
    const animationDuration = 800;
    animateCounter(animationDuration);
};

addCount.addEventListener('click', incrementCounter);

const decrementAnimationDuration = 500;

const decrementCounter = () => {
    count--;
    animateCounter(decrementAnimationDuration);
};

lowerCount.addEventListener('click', decrementCounter);

animateCounter(decrementAnimationDuration);
